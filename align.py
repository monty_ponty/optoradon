import os
import gdal
import gc
import osr
from scipy.misc import imresize
import pickle
from PIL import Image
from skimage import exposure
import numpy as np
from osgeo import gdal, gdalnumeric, ogr, osr, gdal_array
from gdalconst import GA_ReadOnly
from skimage.measure import block_reduce
import matplotlib.pyplot as plt


def get_geo_info( filename ):
    ''' Gets information from a Raster data set
    '''
    sourceds = gdal.Open(filename, GA_ReadOnly)
    ndv = sourceds.GetRasterBand(1).GetNoDataValue()
    xsize = sourceds.RasterXSize
    ysize = sourceds.RasterYSize
    geot = sourceds.GetGeoTransform()
    projection = osr.SpatialReference()
    projection.ImportFromWkt(sourceds.GetProjectionRef())
    datatype = sourceds.GetRasterBand(1).DataType
    datatype = gdal.GetDataTypeName(datatype)
    return ndv, xsize, ysize, geot, projection, datatype


def map_pixel( point_x, point_y, xpixel_size, ypixel_size, geoBox, maxrow, maxcol ):
    '''
    Usage: map_pixel(xcoord, ycoord, x_cell_size, y_cell_size, xmin, ymax)
    where:
            xmin is leftmost X coordinate in system
            ymax is topmost Y coordinate in system
    Example:
            raster = HMISea.tif'
            ndv, xsize, ysize, geot, projection, datatype = GetGeoInfo(raster)
            row, col = map_pixel(x,y,geot[1],geot[-1], geot[0],geot[3])
    '''
    col = np.floor((point_x - geoBox[ 'W' ]) / xpixel_size).astype(int)
    row = np.floor((geoBox[ 'N' ] - point_y) / ypixel_size).astype(int)

    if np.floor((point_x - geoBox[ 'W' ]) / xpixel_size) == 0.:
        col = 0
    if np.floor((point_x - geoBox[ 'E' ]) / xpixel_size) == 0.:
        col = maxcol
    if np.floor((point_y - geoBox[ 'S' ]) / ypixel_size) == 0.:
        row = maxrow
    if np.floor((point_y - geoBox[ 'N' ]) / ypixel_size) == 0.:
        row = 0

    return row, col


'''
def align_rasters( raster, alignraster, interp='nearest' ):

    ndv1, xsize1, ysize1, geot1, projection1, datatype1 = get_geo_info(raster)  # GetGeoInfo(raster)
    ndv2, xsize2, ysize2, geot2, projection2, datatype2 = get_geo_info(alignraster)  # GetGeoInfo(alignraster)
    if projection1.ExportToMICoordSys() == projection2.ExportToMICoordSys():

        #           West              East
        geoBox1 = { 'W': geot1[ 0 ], 'E': geot1[ 0 ] + xsize1 * geot1[ 1 ], \
                    # South                                  North
                    'S': geot1[ 3 ] + ysize1 * geot1[ -1 ], 'N': geot1[ 3 ] }
        geoBox2 = { 'W': geot2[ 0 ], 'E': geot2[ 0 ] + xsize2 * geot2[ 1 ], \
                    'S': geot2[ 3 ] + ysize2 * geot2[ -1 ], 'N': geot2[ 3 ] }
        intersection = { 'W': np.max([ geoBox1[ 'W' ], geoBox2[ 'W' ] ]), \
                         'E': np.min([ geoBox1[ 'E' ], geoBox2[ 'E' ] ]), \
                         'S': np.max([ geoBox1[ 'S' ], geoBox2[ 'S' ] ]), \
                         'N': np.min([ geoBox1[ 'N' ], geoBox2[ 'N' ] ]) }
        print(geoBox1)
        print(geoBox2)
        print(intersection)
        # Resample
        scaleFactor = (geot1[ 1 ] / geot2[ 1 ], geot1[ -1 ] / geot2[ -1 ])
        new_xsize1, new_ysize1 = \
            int(np.round(scaleFactor[ 0 ] * xsize1)), int(np.round(scaleFactor[ 1 ] * ysize1))
        print('New size of raster1 {} {}'.format(new_ysize1, new_xsize1))
        xpixel_size = geot2[ 1 ]
        ypixel_size = -geot2[ -1 ]
        print('\n')
        top1, left1 = map_pixel(intersection[ 'W' ], intersection[ 'N' ], xpixel_size, ypixel_size, \
                                geoBox1, new_ysize1, new_xsize1)
        bottom1, right1 = map_pixel(intersection[ 'E' ], intersection[ 'S' ], xpixel_size, ypixel_size, \
                                    geoBox1, new_ysize1, new_xsize1)
        print('{} {} {} {}'.format(top1, left1, bottom1, right1))
        top2, left2 = map_pixel(intersection[ 'W' ], intersection[ 'N' ], xpixel_size, ypixel_size, \
                                geoBox2, ysize2, xsize2)
        bottom2, right2 = map_pixel(intersection[ 'E' ], intersection[ 'S' ], xpixel_size, ypixel_size, \
                                    geoBox2, ysize2, xsize2)
        print('{} {} {} {}'.format(top2, left2, bottom2, right2))

        img1 = gdalnumeric.LoadFile(raster)
        img1 = imresize(img1, (new_ysize1, new_xsize1), interp=interp)
        img1 = img1[ top1:bottom1, left1:right1 ]
        img2 = gdalnumeric.LoadFile(alignraster)
        img2 = img2[ top2:bottom2, left2:right2 ]
        geot = (geot1[ 0 ], xpixel_size, geot1[ 2 ], geot1[ 3 ], geot1[ 4 ], -ypixel_size)
        return (img1, img2, geot)
    else:
        print("Rasters need to be in same projection")
        return (-1, -1, -1)
'''

dirs = [
    'S1A_IW_GRDH_1SDV_20161203T041655_20161203T041720_014210_016F78_137B',
    'S1A_IW_GRDH_1SDV_20161212T225901_20161212T225926_014353_017411_34E2',
    'S1B_IW_GRDH_1SDV_20160926T093116_20160926T093141_002238_003C32_1C9F',
    'S1B_IW_GRDH_1SDV_20161217T172223_20161217T172248_003439_005DFD_BA24',
    'S1B_IW_GRDH_1SDV_20161218T001528_20161218T001553_003443_005E1E_6770',
    'S1B_IW_GRDH_1SDV_20161218T162229_20161218T162254_003453_005E5D_193F'
]

for dir in dirs:
    print(dir)
    folder = '/home/dmitriy/projects/OpticSARMatching/radar/'

    rastervv = os.path.join(folder, dir, 'vv.tif')
    rastervh = os.path.join(folder, dir, 'vh.tif')
    rasteropt = os.path.join(folder, dir, 'opt.tif')

    ndv1, xsize1, ysize1, geot1, projection1, datatype1 = get_geo_info(rastervh)  # GetGeoInfo(raster)
    ndv2, xsize2, ysize2, geot2, projection2, datatype2 = get_geo_info(
        rasteropt)  # GetGeoInfo(alignraster)  # West              East
    geoBox1 = { 'W': geot1[ 0 ], 'E': geot1[ 0 ] + xsize1 * geot1[ 1 ], \
                # South                                  North
                'S': geot1[ 3 ] + ysize1 * geot1[ -1 ], 'N': geot1[ 3 ] }
    geoBox2 = { 'W': geot2[ 0 ], 'E': geot2[ 0 ] + xsize2 * geot2[ 1 ], \
                'S': geot2[ 3 ] + ysize2 * geot2[ -1 ], 'N': geot2[ 3 ] }
    intersection = { 'W': np.max([ geoBox1[ 'W' ], geoBox2[ 'W' ] ]), \
                     'E': np.min([ geoBox1[ 'E' ], geoBox2[ 'E' ] ]), \
                     'S': np.max([ geoBox1[ 'S' ], geoBox2[ 'S' ] ]), \
                     'N': np.min([ geoBox1[ 'N' ], geoBox2[ 'N' ] ]) }
    print(geoBox1)
    print(geoBox2)
    print(intersection)
    # Resample
    scaleFactor = (geot1[ 1 ] / geot2[ 1 ], geot1[ -1 ] / geot2[ -1 ])
    new_xsize1, new_ysize1 = \
        int(np.round(scaleFactor[ 0 ] * xsize1)), int(np.round(scaleFactor[ 1 ] * ysize1))
    print('New size of raster1 {} {}'.format(new_ysize1, new_xsize1))
    xpixel_size = geot2[ 1 ]
    ypixel_size = -geot2[ -1 ]
    print('\n')
    top1, left1 = map_pixel(intersection[ 'W' ], intersection[ 'N' ], xpixel_size, ypixel_size, \
                            geoBox1, new_ysize1, new_xsize1)
    bottom1, right1 = map_pixel(intersection[ 'E' ], intersection[ 'S' ], xpixel_size, ypixel_size, \
                                geoBox1, new_ysize1, new_xsize1)
    print('{} {} {} {}'.format(top1, left1, bottom1, right1))
    top2, left2 = map_pixel(intersection[ 'W' ], intersection[ 'N' ], xpixel_size, ypixel_size, \
                            geoBox2, ysize2, xsize2)
    bottom2, right2 = map_pixel(intersection[ 'E' ], intersection[ 'S' ], xpixel_size, ypixel_size, \
                                geoBox2, ysize2, xsize2)
    print('{} {} {} {}'.format(top2, left2, bottom2, right2))

    print('Reading SAR rasters')
    imgvv = gdalnumeric.LoadFile(rastervv)
    imgvh = gdalnumeric.LoadFile(rastervh)
    print('Transforming SAR')
    img1 = np.sqrt(np.square(imgvv) + np.square(imgvh))
    del imgvv
    del imgvh
    gc.collect()
    img1 = img1 / img1.max()
    print('Resizing')
    img1 = imresize(img1, (new_ysize1, new_xsize1), interp='bilinear')
    img1 = img1[ top1:bottom1, left1:right1 ]
    print('Writing')
    with open(os.path.join(folder, dir, 'aligned_SAR.pickle'), 'wb') as f:
        pickle.dump(img1, f)
    gc.collect()
    print('Done!\n\n')
    '''
    img2 = gdalnumeric.LoadFile(alignraster)
    img2 = img2[ top2:bottom2, left2:right2 ]
    geot = (geot1[ 0 ], xpixel_size, geot1[ 2 ], geot1[ 3 ], geot1[ 4 ], -ypixel_size)
    '''
'''
ndv=0
for dir in dirs:
    gc.collect()
    print(dir)
    with open(os.path.join(folder, dir, 'aligned_SAR.pickle'), 'rb') as f:
        arr = pickle.load(f)
    rows, cols = arr.shape
    top = 0
    left = 0
    right = cols - 1
    bottom = rows - 1
    while arr[ top ].max() == ndv: top += 1
    while arr[ :, left ].max() == ndv: left += 1
    while arr[ bottom ].max() == ndv: bottom -= 1
    while arr[ :, right ].max() == ndv: right -= 1
    curLeftTopPoint = np.array((top, arr[ top ].argmax()))
    curLeftBottomPoint = np.array((arr[ :, left ].argmax(), left))
    curRightTopPoint = np.array((arr[ :, right ].argmax(), right))
    curRightBottomPoint = np.array((bottom, arr[ bottom ].argmax()))
    x = np.array([ curLeftTopPoint[ 0 ], curRightBottomPoint[ 0 ], curLeftBottomPoint[ 0 ], curRightTopPoint[ 0 ] ])
    y = np.array([ curLeftTopPoint[ 1 ], curRightBottomPoint[ 1 ], curLeftBottomPoint[ 1 ], curRightTopPoint[ 1 ] ])
    x.sort()
    y.sort()

    img = Image.fromarray(arr[x[1]:x[2],y[1]:y[2]])
    del arr
    gc.collect()
    img.save(os.path.join(folder,dir, 'cropped_SAR.png'), 'PNG')
    arr= gdalnumeric.LoadFile(os.path.join(folder,dir, 'opt.tif'))
    arr=np.swapaxes(arr,0,2)
    arr=np.swapaxes(arr,0,1)
    img = Image.fromarray(arr[ x[ 1 ]:x[ 2 ], y[ 1 ]:y[ 2 ],: ])
    img.save(os.path.join(folder, dir, 'cropped_OPT.png'), 'PNG')
'''
for dir in dirs:
    print(dir)
    folder = '/home/dmitriy/projects/OpticSARMatching/radar/'

