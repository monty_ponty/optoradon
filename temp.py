import os
import zipfile
import gdal
import gc
import cv2
import numpy as np
import matplotlib.pyplot as plt
import utm
import osr
import pyproj
from scipy.misc import imresize
from PIL import Image
from skimage import exposure
import numpy as np
from osgeo import gdal, gdalnumeric, ogr, osr, gdal_array
from gdalconst import GA_ReadOnly
from skimage.measure import block_reduce


def get_geo_info( filename ):
    ''' Gets information from a Raster data set
    '''
    sourceds = gdal.Open(filename, GA_ReadOnly)
    ndv = sourceds.GetRasterBand(1).GetNoDataValue()
    xsize = sourceds.RasterXSize
    ysize = sourceds.RasterYSize
    geot = sourceds.GetGeoTransform()
    projection = osr.SpatialReference()
    projection.ImportFromWkt(sourceds.GetProjectionRef())
    datatype = sourceds.GetRasterBand(1).DataType
    datatype = gdal.GetDataTypeName(datatype)
    return ndv, xsize, ysize, geot, projection, datatype


def map_pixel( point_x, point_y, xpixel_size, ypixel_size, geoBox, maxrow, maxcol ):
    '''
    Usage: map_pixel(xcoord, ycoord, x_cell_size, y_cell_size, xmin, ymax)
    where:
            xmin is leftmost X coordinate in system
            ymax is topmost Y coordinate in system
    Example:
            raster = HMISea.tif'
            ndv, xsize, ysize, geot, projection, datatype = GetGeoInfo(raster)
            row, col = map_pixel(x,y,geot[1],geot[-1], geot[0],geot[3])
    '''
    col = np.floor((point_x - geoBox[ 'W' ]) / xpixel_size).astype(int)
    row = np.floor((geoBox[ 'N' ] - point_y) / ypixel_size).astype(int)

    if np.floor((point_x - geoBox[ 'W' ]) / xpixel_size) == 0.:
        col = 0
    if np.floor((point_x - geoBox[ 'E' ]) / xpixel_size) == 0.:
        col = maxcol
    if np.floor((point_y - geoBox[ 'S' ]) / ypixel_size) == 0.:
        row = maxrow
    if np.floor((point_y - geoBox[ 'N' ]) / ypixel_size) == 0.:
        row = 0

    return row, col


def align_rasters( raster, alignraster, interp='nearest' ):
    '''
    Align two rasters so that data overlaps by geographical location
    Usage:
    (alignedraster_o, alignedraster_a, geot_a) = AlignRasters(raster, alignraster, how=np.mean)
    where
        raster: string with location of raster to be aligned
        alignraster: string with location of raster to which raster will be aligned
        how: function used to aggregate cells (if the rasters have different sizes)
    It is assumed that both rasters have the same size
    '''
    ndv1, xsize1, ysize1, geot1, projection1, datatype1 = get_geo_info(raster)  # GetGeoInfo(raster)
    ndv2, xsize2, ysize2, geot2, projection2, datatype2 = get_geo_info(alignraster)  # GetGeoInfo(alignraster)
    if projection1.ExportToMICoordSys() == projection2.ExportToMICoordSys():

        #           West              East
        geoBox1 = { 'W': geot1[ 0 ], 'E': geot1[ 0 ] + xsize1 * geot1[ 1 ], \
                    # South                                  North
                    'S': geot1[ 3 ] + ysize1 * geot1[ -1 ], 'N': geot1[ 3 ] }
        geoBox2 = { 'W': geot2[ 0 ], 'E': geot2[ 0 ] + xsize2 * geot2[ 1 ], \
                    'S': geot2[ 3 ] + ysize2 * geot2[ -1 ], 'N': geot2[ 3 ] }
        intersection = { 'W': np.max([ geoBox1[ 'W' ], geoBox2[ 'W' ] ]), \
                         'E': np.min([ geoBox1[ 'E' ], geoBox2[ 'E' ] ]), \
                         'S': np.max([ geoBox1[ 'S' ], geoBox2[ 'S' ] ]), \
                         'N': np.min([ geoBox1[ 'N' ], geoBox2[ 'N' ] ]) }
        print(geoBox1)
        print(geoBox2)
        print(intersection)
        # Resample
        scaleFactor = (geot1[ 1 ] / geot2[ 1 ], geot1[ -1 ] / geot2[ -1 ])
        new_xsize1, new_ysize1 = \
            int(np.round(scaleFactor[ 0 ] * xsize1)), int(np.round(scaleFactor[ 1 ] * ysize1))
        print('New size of raster1 {} {}'.format(new_ysize1, new_xsize1))
        xpixel_size = geot2[ 1 ]
        ypixel_size = -geot2[ -1 ]
        print('\n')
        top1, left1 = map_pixel(intersection[ 'W' ], intersection[ 'N' ], xpixel_size, ypixel_size, \
                                geoBox1, new_ysize1, new_xsize1)
        bottom1, right1 = map_pixel(intersection[ 'E' ], intersection[ 'S' ], xpixel_size, ypixel_size, \
                                    geoBox1, new_ysize1, new_xsize1)
        print('{} {} {} {}'.format(top1, left1, bottom1, right1))
        top2, left2 = map_pixel(intersection[ 'W' ], intersection[ 'N' ], xpixel_size, ypixel_size, \
                                geoBox2, ysize2, xsize2)
        bottom2, right2 = map_pixel(intersection[ 'E' ], intersection[ 'S' ], xpixel_size, ypixel_size, \
                                    geoBox2, ysize2, xsize2)
        print('{} {} {} {}'.format(top2, left2, bottom2, right2))

        img1 = gdalnumeric.LoadFile(raster)
        img1 = imresize(img1, (new_ysize1, new_xsize1), interp=interp)
        img1 = img1[ top1:bottom1, left1:right1 ]
        img2 = gdalnumeric.LoadFile(alignraster)
        img2 = img2[ top2:bottom2, left2:right2 ]
        geot = (geot1[ 0 ], xpixel_size, geot1[ 2 ], geot1[ 3 ], geot1[ 4 ], -ypixel_size)
        return (img1, img2, geot)
    else:
        print("Rasters need to be in same projection")
        return (-1, -1, -1)


'''
folder='/home/dmitriy/projects/OpticSARMatching/radar'
's1a-iw-grd-vv-20161212t225901-20161212t225926-014353-017411-001.tiff'
for filename in os.listdir(folder):
    if filename.endswith(".zip"):
        zipOb =zipfile.ZipFile(folder+'/'+filename, "r")
        for name in zip.getnames():
            print(name)
'''
folder = '/home/dmitriy/projects/OpticSARMatching/radar/'
imagename = 's1a-iw-grd-vh-20161203t041655-20161203t041720-014210-016f78-002.tiff'
dataset = gdal.Open(folder + imagename)

print('Driver: ', dataset.GetDriver().ShortName, '/', \
      dataset.GetDriver().LongName)
print('Size is ', dataset.RasterXSize, 'x', dataset.RasterYSize, \
      'x', dataset.RasterCount)
print('Projection is ', dataset.GetProjection())
geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print('Origin = (', geotransform[ 0 ], ',', geotransform[ 3 ], ')')
    print('Pixel Size = (', geotransform[ 1 ], ',', geotransform[ 5 ], ')')

band = dataset.GetRasterBand(1)
print('Band Type=', gdal.GetDataTypeName(band.DataType))
min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
    (min, max) = band.ComputeRasterMinMax(1)
print('Min=%.3f, Max=%.3f' % (min, max))
if band.GetOverviewCount() > 0:
    print('Band has ', band.GetOverviewCount(), ' overviews.')
if not band.GetRasterColorTable() is None:
    print('Band has a color table with ', \
          band.GetRasterColorTable().GetCount(), ' entries.')
'''
import osr
from pyproj import Proj, transform
srs=osr.SpatialReference()
srs.ImportFromWkt(dataset.GetProjectionRef())
inProj = Proj(str(srs.ExportToProj4()))
outProj = Proj(init='epsg:4326')
x1,y1 =  geotransform[0], geotransform[3]
lon,lat = transform(inProj,outProj,x1,y1)
print (lat,lon)

for filename in os.listdir(folder):
    if filename.endswith(".TIF"):
        dataset = gdal.Open(folder + filename)
        geotransform = dataset.GetGeoTransform()
        srs = osr.SpatialReference()
        print(folder + filename)
        srs.ImportFromWkt(dataset.GetProjection())
        inProj = Proj(str(srs.ExportToProj4()))
        outProj = Proj(init='epsg:4326')
        x1, y1 = geotransform[ 0 ], geotransform[ 3 ]
        lon, lat = transform(inProj, outProj, x1, y1)
        print(lat, lon)
'''
import osr
from pyproj import Proj, transform

srs = osr.SpatialReference()
wkt = dataset.GetGCPProjection()
srs.ImportFromWkt(wkt)

imagenames = [ 's1a-iw-grd-vh-20161203t041655-20161203t041720-014210-016f78-002.tiff', \
               's1a-iw-grd-vv-20161203t041655-20161203t041720-014210-016f78-001.tiff' ]
dataset = gdal.Open(folder + imagenames[ 0 ])
arr1 = dataset.ReadAsArray().astype('float32')
dataset = gdal.Open(folder + imagenames[ 1 ])
arr2 = dataset.ReadAsArray().astype('float32')
arr = np.sqrt(arr1 ** 2 + arr2 ** 2)

dirs = [
    'S1A_IW_GRDH_1SDV_20161212T225901_20161212T225926_014353_017411_34E2', \
    'S1A_IW_GRDH_1SDV_20161203T041655_20161203T041720_014210_016F78_137B', \
    'S1B_IW_GRDH_1SDV_20161218T001528_20161218T001553_003443_005E1E_6770', \
    'S1B_IW_GRDH_1SDV_20161217T172223_20161217T172248_003439_005DFD_BA24', \
    'S1B_IW_GRDH_1SDV_20161215T174001_20161215T174026_003410_005D2D_1EDA', \
    'S1B_IW_GRDH_1SDV_20160926T093116_20160926T093141_002238_003C32_1C9F', \
    'S1B_IW_GRDH_1SDV_20161213T201909_20161213T201934_003382_005C6A_14EE', \
    'S1B_IW_GRDH_1SDV_20161218T162229_20161218T162254_003453_005E5D_193F'
]
folder = '/home/dmitriy/projects/OpticSARMatching/radar/'
for dir in dirs:
    f = folder + dir + '/' + 'measurement/'
    print(folder + dir + '/')
    dataset = None
    for filename in os.listdir(f):
        dataset = gdal.Open(os.path.join(f, filename))
        break
    arr = dataset.ReadAsArray().astype('float32')
    arr = np.log(1 + arr)
    arr = arr / arr.max() * 255
    arr = arr.astype('uint8')
    img = Image.fromarray(arr)
    img.save(folder + dir + '/' + 'preview.jpg', 'JPEG', quality=80)
    gc.collect()

'''
def saveAsJpeg( arr, folder, name ):
    arr = exposure.rescale_intensity(arr, out_range=(-1, 1))
    arr = exposure.equalize_adapthist(arr)
    arr = exposure.rescale_intensity(arr, out_range=(0, 255))
    gc.collect()
    img = Image.fromarray(arr.astype('uint8'))
    img.save(os.path.join(folder, name), 'JPEG', quality=80)
    gc.collect()
    print('Saved as {}'.format(os.path.join(folder, name)))

'''
'''
for blockSize in range(16, 1, -1):
    print('BlockSize: {}'.format(blockSize))
    dataset = gdal.Open(os.path.join(f, filename))
    arr = dataset.ReadAsArray()
    arr = block_reduce(arr, block_size=(blockSize, blockSize), func=np.mean)
    arr = arr[ :1024, :1024 ]
    arr = arr / arr.max()
    arr1 = exposure.equalize_hist(arr) * 255
    arr1 = arr1.astype('uint8')
    img = Image.fromarray(arr1)
    img.save(os.path.join(folder, 'crop_down_by_' + str(blockSize) + '.jpg'), 'JPEG', quality=80)
    gc.collect()
    arr2 = exposure.equalize_adapthist(arr) * 255
    arr2 = arr2.astype('uint8')
    img = Image.fromarray(arr2)
    img.save(os.path.join(folder, 'crop_adapt_down_by_' + str(blockSize) + '.jpg'), 'JPEG', quality=80)
'''


def rotate_image( mat, angle ):
    # angle in degrees

    height, width = mat.shape[ :2 ]
    image_center = ((width - 1) / 2, (height - 1) / 2)

    rotation_mat = cv2.getRotationMatrix2D(image_center, angle, 1.)

    abs_cos = abs(rotation_mat[ 0, 0 ])
    abs_sin = abs(rotation_mat[ 0, 1 ])

    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    rotation_mat[ 0, 2 ] += bound_w / 2 - image_center[ 0 ]
    rotation_mat[ 1, 2 ] += bound_h / 2 - image_center[ 1 ]

    rotated_mat = cv2.warpAffine(mat, rotation_mat, (bound_w, bound_h))
    return rotated_mat


blockSize = 16
dataset = gdal.Open(os.path.join(f, filename))
arr = dataset.ReadAsArray()
arr = block_reduce(arr, block_size=(blockSize, blockSize), func=np.mean)
arr = arr / arr.max()
arr = exposure.equalize_hist(arr)

geoCtrlPoints = { (gcp.GCPLine, gcp.GCPPixel): [ gcp.GCPY, gcp.GCPX ] for gcp in dataset.GetGCPs() }
rowRange = (0, dataset.RasterYSize - 1)
colRange = (0, dataset.RasterXSize - 1)
topLeftLL = geoCtrlPoints[ (rowRange[ 0 ], colRange[ 0 ]) ]
topRightLL = geoCtrlPoints[ (rowRange[ 0 ], colRange[ 1 ]) ]
bottomLeftLL = geoCtrlPoints[ (rowRange[ 1 ], colRange[ 0 ]) ]
bottomRightLL = geoCtrlPoints[ (rowRange[ 1 ], colRange[ 1 ]) ]

topLeft, topRight, bottomLeft, bottomRight = [ 0, 0 ], [ 0, 0 ], [ 0, 0 ], [ 0, 0 ]
topLeft[ 1 ], topLeft[ 0 ], _, _ = utm.from_latlon(topLeftLL[ 0 ], topLeftLL[ 1 ])
topRight[ 1 ], topRight[ 0 ], _, _ = utm.from_latlon(topRightLL[ 0 ], topRightLL[ 1 ])
bottomLeft[ 1 ], bottomLeft[ 0 ], _, _ = utm.from_latlon(bottomLeftLL[ 0 ], bottomLeftLL[ 1 ])
bottomRight[ 1 ], bottomRight[ 0 ], _, _ = utm.from_latlon(bottomRightLL[ 0 ], bottomRightLL[ 1 ])

north = np.max([ topLeft[ 0 ], topRight[ 0 ] ])
south = np.min([ bottomLeft[ 0 ], bottomRight[ 0 ] ])
if north < south:
    topLeft[ 0 ], topRight[ 0 ], bottomLeft[ 0 ], bottomRight[ 0 ] = \
        bottomLeft[ 0 ], bottomRight[ 0 ], topLeft[ 0 ], topRight[ 0 ]
    topLeftLL[ 0 ], topRightLL[ 0 ], bottomLeftLL[ 0 ], bottomRightLL[ 0 ] = \
        bottomLeftLL[ 0 ], bottomRightLL[ 0 ], topLeftLL[ 0 ], topRightLL[ 0 ]
    north = np.max([ topLeft[ 0 ], topRight[ 0 ] ])
    south = np.min([ bottomLeft[ 0 ], bottomRight[ 0 ] ])
    arr = np.flipud(arr)
northLL = np.max([ topLeftLL[ 0 ], topRightLL[ 0 ] ])
southLL = np.min([ bottomLeftLL[ 0 ], bottomRightLL[ 0 ] ])

east = np.max([ topRight[ 1 ], bottomRight[ 1 ] ])
west = np.min([ topLeft[ 1 ], bottomLeft[ 1 ] ])
if east < west:
    topRight[ 1 ], bottomRight[ 1 ], topLeft[ 1 ], bottomLeft[ 1 ] = \
        topLeft[ 1 ], bottomLeft[ 1 ], topRight[ 1 ], bottomRight[ 1 ]
    topRightLL[ 1 ], bottomRightLL[ 1 ], topLeftLL[ 1 ], bottomLeftLL[ 1 ] = \
        topLeftLL[ 1 ], bottomLeftLL[ 1 ], topRightLL[ 1 ], bottomRightLL[ 1 ]
    east = np.max([ topRight[ 1 ], bottomRight[ 1 ] ])
    west = np.min([ topLeft[ 1 ], bottomLeft[ 1 ] ])
    arr = np.fliplr(arr)
eastLL = np.max([ topRightLL[ 1 ], bottomRightLL[ 1 ] ])
westLL = np.min([ topLeftLL[ 1 ], bottomLeftLL[ 1 ] ])

tangent1 = -(topLeft[ 1 ] - west) / (bottomLeft[ 0 ] - north)
tangent2 = -(bottomRight[ 1 ] - east) / (topRight[ 0 ] - south)

angle = np.degrees(np.arctan((tangent1 + tangent2) / 2))

transformed = rotate_image(arr, angle)
transformed = transformed / transformed.max() * 255
transformed = transformed.astype('uint8')
'''
img = Image.fromarray(transformed)
img.save(os.path.join(folder, 'rotated.jpg'), 'JPEG', quality=80)
gc.collect()
'''
centreLat = (topLeftLL[ 0 ] + topRightLL[ 0 ] + bottomLeftLL[ 0 ] + bottomRightLL[ 0 ]) / 4
centreLong = (topRightLL[ 1 ] + bottomRightLL[ 1 ] + topLeftLL[ 1 ] + bottomLeftLL[ 1 ]) / 4
easting, northing, zone, zoneL = utm.from_latlon(centreLat, centreLong)

utm_coordinate_system = osr.SpatialReference()
utm_coordinate_system.SetWellKnownGeogCS("WGS84")  # Set geographic coordinate system to handle lat/lon
is_northern = northing > 0
utm_coordinate_system.SetUTM(zone, is_northern)
wkt_projection = utm_coordinate_system.ExportToWkt()

dst_filename = os.path.join(folder, 'myGeo.tiff')

x_pixels = transformed.shape[ 1 ]
y_pixels = transformed.shape[ 0 ]

driver = gdal.GetDriverByName('GTiff')

dataset = driver.Create(
    dst_filename,
    x_pixels,
    y_pixels,
    1,
    gdal.GDT_Int16, )

dataset.SetGeoTransform((
    west,  # 0
    (east - west) / x_pixels,  # 1
    0,  # 2
    north,  # 3
    0,  # 4
    (south - north) / y_pixels))

dataset.SetProjection(wkt_projection)
dataset.GetRasterBand(1).WriteArray(transformed)
dataset.FlushCache()  # Write to disk.
