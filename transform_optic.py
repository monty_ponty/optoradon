import os
import gdal
import utm
from pyproj import Proj, transform

inFolder='/media/dmitriy/TRANSCEND4G'
inFile='optic.tif'
outFolder='/home/dmitriy/projects/OpticSARMatching/radar/'
outFile='optic.tif'
input=os.path.join(inFolder,inFile)
output=os.path.join(outFolder,outFile)

dataset=gdal.Open(os.path.join(inFolder,inFile))
geot=dataset.GetGeoTransform()

inProj = Proj(init='epsg:3785')
outProj = Proj(init='epsg:4326')
x1,y1 = geot[0],geot[3]
x2,y2 = transform(inProj,outProj,x1,y1)
_,_, zone, zoneL=utm.from_latlon(y2,x2)


os.system('gdalwarp '+input+' '+output+' -t_srs "+proj=utm +zone='+str(zone)+zoneL+' +datum=WGS84"')