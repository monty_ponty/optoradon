import pickle
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import cv2



def keypoint_extractor( img1, img2, toldist=3, hessianThreshold=1000, nOctaves=4, nOctaveLayers=2 ):
    detector = cv2.xfeatures2d.SURF_create(hessianThreshold, nOctaves, nOctaveLayers)
    kp1, desc1 = detector.detectAndCompute(img1, None)
    kp2, desc2 = detector.detectAndCompute(img2, None)
    x1 = np.array([ keypoint.pt[ 0 ] for keypoint in kp1 ])
    y1 = np.array([ keypoint.pt[ 1 ] for keypoint in kp1 ])
    x2 = np.array([ keypoint.pt[ 0 ] for keypoint in kp2 ])
    y2 = np.array([ keypoint.pt[ 1 ] for keypoint in kp2 ])
    pairs = { }
    for i in range(len(x1)):
        dist = np.sqrt(np.square(x2 - x1[ i ]) + np.square(y2 - y1[ i ]))
        j = dist.argmin()
        if dist[ j ] < toldist:
            pairs[ i ] = j
    keypoints1 = [ ]
    keypoints2 = [ ]
    descriptors1 = [ ]
    descriptors2 = [ ]
    for i in pairs:
        keypoints1.append(kp1[ i ])
        keypoints2.append(kp2[ pairs[ i ] ])
        descriptors1.append(desc1[ i ])
        descriptors2.append(desc2[ pairs[ i ] ])
    return (keypoints1, descriptors1), (keypoints2, descriptors2)


folder = 'E:\\preview\\'
img1 = cv2.imread(folder+'patch_OPT190.png',0)
img2 = cv2.imread(folder+'patch_SAR190.png',0)
one,two=keypoint_extractor(img1,img2,toldist=3)
im1 = cv2.drawKeypoints(img1,one[0],None,(255,0,0),4)
plt.imshow(im1)
plt.figure()
im2 = cv2.drawKeypoints(img2,two[0],None,(255,0,0),4)
plt.imshow(im2)