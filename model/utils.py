import os
import random
import numpy as np
import gc
import pickle
import lasagne as nn

data_dir = '/home/dmitriy/projects/MetricLearning/'


def unpickle( file ):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo)
    return dict


def load_data( ):
    test = [ ]
    train = [ ]
    for j in [ 1, 2 ]:
        print(j)
        d = unpickle(os.path.join(data_dir, 'batches_32x32/location_' + str(j)))
        test.append(d[ 'data' ].astype('float32'))
        gc.collect()

    for j in range(2 + 1, 6 + 1):
        print(j)
        d = unpickle(os.path.join(data_dir, 'batches_32x32/location_' + str(j)))
        train.append(d[ 'data' ].astype('float32'))
        gc.collect()

    test = np.concatenate(test, axis=0).astype('float32')
    train = np.concatenate(train, axis=0).astype('float32')
    test-=test.mean(axis=0)
    test/=test.std(axis=0)
    train-=train.mean(axis=0)
    train/=train.std(axis=0)
    return train, test


def nonrep_random( max ):
    '''
    Non-repeating random int generator
    max - excluded
    '''
    number = random.randint(0, max - 1)
    while True:
        number = (number + random.randint(1, max - 1)) % max
        yield number


def create_pairs( patches, target_ratio=0.5, max_count=None ):
    patches_num = patches.shape[ 0 ]
    if max_count == None:
        max_count = int(patches_num / target_ratio)
    count = 1
    targetCount = 0
    rand = nonrep_random(patches_num)
    idx = np.random.permutation(patches_num).tolist()
    while (count < max_count + 1) and idx:
        if (targetCount / count < target_ratio):
            index = idx.pop()
            yield (patches[ index, 0 ], patches[ index, 1 ], 1)
            targetCount += 1
        else:
            yield (patches[ rand.__next__(), 0 ], patches[ rand.__next__(), 1 ], 0)
        count += 1


def get_train_test_pairs( train,test,target_ratio=0.5, max_count=None ):

    return create_pairs(train, target_ratio, max_count), \
           create_pairs(test, target_ratio, max_count)


def iterate_minibatches( pairs, batchsize ):
    a, b, targets = [ ], [ ], [ ]
    count = 0
    for (_a, _b, _target) in pairs:
        a.append(_a)
        b.append(_b)
        targets.append(_target)
        count += 1
        if count == batchsize:
            yield np.expand_dims(np.stack(a), axis=1), np.expand_dims(np.stack(b), axis=1), np.int32(targets)
            a, b, targets = [ ], [ ], [ ]
            count = 0
    yield np.expand_dims(np.stack(a),axis=1), np.expand_dims(np.stack(b),axis=1), np.int32(targets)


PARAM_EXTENSION = 'params'


def read_model_data(model, filename):
    """Unpickles and loads parameters into a Lasagne model."""
    filename = os.path.join('%s.%s' % (filename, PARAM_EXTENSION))
    with open(filename, 'rb') as f:
        data = pickle.load(f)
    nn.layers.set_all_param_values(model, data)


def write_model_data(model, filename):
    """Pickels the parameters within a Lasagne model."""
    data = nn.layers.get_all_param_values(model)
    filename = '%s.%s' % (filename, PARAM_EXTENSION)
    with open(filename, 'wb') as f:
        pickle.dump(data, f)
