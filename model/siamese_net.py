#THEANO_FLAGS=device=gpu0,nvcc.flags=-D_FORCE_INLINES
import numpy as np
import os
os.environ["THEANO_FLAGS"] = "device=gpu0,nvcc.flags=-D_FORCE_INLINES"
import time
import lasagne
import theano
import pandas as pd

from lasagne.layers import DenseLayer
from lasagne.layers import InputLayer
from lasagne.nonlinearities import rectify, linear, sigmoid
from theano import tensor as T

import utils

net_dir = '/home/dmitriy/projects/MetricLearning/net3'


def euclidean_distance( x, y ):
    return T.sqrt(T.sum(T.square(x - y), axis=1, keepdims=True))


def contrastive_loss( y_true, y_pred ):
    margin = 1
    return T.mean(y_true * T.square(y_pred) + (1 - y_true) * T.square(T.maximum(margin - y_pred, 0)))


def build_cnn( input_var=None ):
    network = InputLayer(shape=(None, 1, 32, 32), input_var=input_var)
    network = lasagne.layers.Conv2DLayer(
        network, num_filters=32,
        filter_size=(3, 3),
        nonlinearity=lasagne.nonlinearities.rectify,
        W=lasagne.init.GlorotUniform(gain='relu'))
    network = DenseLayer(
        network, num_units=256,
        nonlinearity=rectify)
    network = DenseLayer(
        network, num_units=2,
        nonlinearity=linear)
    return network

def build_2conv_cnn(input_var=None):
    network = lasagne.layers.InputLayer(shape=(None, 1, 32, 32),
                                        input_var=input_var)
    network = lasagne.layers.Conv2DLayer(
            network, num_filters=32, filter_size=(5, 5),
            nonlinearity=lasagne.nonlinearities.rectify,
            W=lasagne.init.GlorotUniform())
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))
    network = lasagne.layers.Conv2DLayer(
            network, num_filters=32, filter_size=(5, 5),
            nonlinearity=lasagne.nonlinearities.rectify)
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))
    network = lasagne.layers.DenseLayer(
            lasagne.layers.dropout(network, p=.5),
            num_units=256,
            nonlinearity=lasagne.nonlinearities.rectify)
    network = lasagne.layers.DenseLayer(
            lasagne.layers.dropout(network, p=.5),
            num_units=3,
            nonlinearity=lasagne.nonlinearities.softmax)

    return network

def main( ):
    lr = 0.1
    num_epochs = 100
    batch_size = 256

    input_var_a = T.tensor4('inputs_a')
    input_var_b = T.tensor4('inputs_b')
    target_var = T.ivector('targets')

    network = build_cnn()

    # train function
    prediction_a = lasagne.layers.get_output(network, input_var_a)
    prediction_b = lasagne.layers.get_output(network, input_var_b)

    d = euclidean_distance(prediction_a, prediction_b)
    loss = contrastive_loss(target_var, d)

    params = lasagne.layers.get_all_params(network, trainable=True)
    updates = lasagne.updates.adadelta(loss, params, learning_rate=lr)

    train_fn = theano.function([ input_var_a, input_var_b, target_var ], loss, updates=updates)

    # test function
    test_prediction_a = lasagne.layers.get_output(network, input_var_a, deterministic=True)
    test_prediction_b = lasagne.layers.get_output(network, input_var_b, deterministic=True)

    test_d = euclidean_distance(test_prediction_a, test_prediction_b)
    test_loss = contrastive_loss(target_var, test_d)

    val_fn = theano.function([ input_var_a, input_var_b, target_var ], test_loss)
    traindata, testdata = utils.load_data()

    df_scores=pd.DataFrame(columns=['train_loss','test_loss','epoch'])
    df_scores.to_csv(os.path.join(net_dir,'scores.csv'))
    for epoch in range(num_epochs):

        tr_pairs, te_pairs = utils.get_train_test_pairs(traindata, testdata, target_ratio=0.1, max_count=205000)
        print("Epoch {} starting training".format(epoch + 1))
        train_err = 0
        train_batches = 0
        iteration = 0
        start_time = time.time()
        for batch in utils.iterate_minibatches(tr_pairs, batch_size):
            a, b, targets = batch
            err = train_fn(a, b, targets)
            train_err += err
            if iteration == 100:
                print('Train error :' + str(err))
                iteration = 0
            iteration += 1
            train_batches += 1

        val_err = 0
        val_batches = 0
        iteration = 0
        for batch in utils.iterate_minibatches(te_pairs, batch_size):
            a, b, targets = batch
            err = val_fn(a, b, targets)
            val_err += err
            if iteration == 100:
                print('Val error :' + str(err))
                iteration = 0
            iteration += 1
            val_batches += 1

        print("Epoch {} of {} took {:.3f}s".format(epoch + 1, num_epochs, time.time() - start_time))
        print("  training loss:\t\t{:.10f}".format(train_err / train_batches))
        print("  validation loss:\t\t{:.10f}".format(val_err / val_batches))
        utils.write_model_data(network, os.path.join(net_dir, 'epoch_' + str(epoch + 1)))
        df_scores.loc[0]=[train_err / train_batches,val_err / val_batches,epoch+1]
        with open(os.path.join(net_dir,'scores.csv'), 'a') as f:
            df_scores.to_csv(f, header=False)


if __name__ == '__main__':
    main()
