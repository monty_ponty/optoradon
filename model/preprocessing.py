import os
import tarfile
import gdal
import sys
import gc
import cv2
import numpy as np
import pickle
import matplotlib.pyplot as plt

_PATCH_SIZE_ = 32


def get_patches( image, size ):
    idx = np.arange(0, image.shape[ 0 ] - size, size)
    row_idx = [ np.arange(j, j + size).tolist() for j in idx ]
    patches = image[ row_idx, slice(0, size) ]
    for col in np.arange(size, image.shape[ 1 ] - size, size):
        patches = np.append(patches, image[ row_idx, slice(col, col + size) ], axis=0)
    return patches


folder = '/home/dmitriy/projects/MetricLearning/'

'''
for filename in os.listdir(folder):
    if filename.endswith(".gz"):
        tar = tarfile.open(folder+filename, "r:gz")
        for name in tar.getnames():
            if name.endswith('_B2.TIF')  or\
                name.endswith('_B3.TIF') or\
                name.endswith('_B4.TIF') or\
                name.endswith('_B11.TIF'):
                tar.extract(name)
'''
imagenames = set()
for filename in os.listdir(folder):
    if filename.endswith(".TIF"):
        imagenames |= set([ filename.split(sep='_')[ 0 ] ])

for imagename, loc_idx in zip(imagenames, range(len(imagenames))):
    print('Processing LOC_' + str(loc_idx) + '\t\t image ' + imagename)
    Htif = gdal.Open(folder + imagename + '_B11.TIF')
    HImg = Htif.ReadAsArray()
    rows, cols = HImg.shape
    top = 0
    left = 0
    right = cols - 1
    bottom = rows - 1
    while HImg[ top ].max() == 0: top += 1
    while HImg[ :, left ].max() == 0: left += 1
    while HImg[ bottom ].max() == 0: bottom -= 1
    while HImg[ :, right ].max() == 0: right -= 1
    curLeftTopPoint = np.array((top, HImg[ top ].argmax()))
    curLeftBottomPoint = np.array((HImg[ :, left ].argmax(), left))
    curRightTopPoint = np.array((HImg[ :, right ].argmax(), right))
    curRightBottomPoint = np.array((bottom, HImg[ bottom ].argmax()))
    angle = np.degrees(np.arctan((curLeftTopPoint[ 1 ] - left) / (curLeftBottomPoint[ 0 ] - top)))
    transformation = cv2.getRotationMatrix2D(((right - left) / 2, (bottom - top) / 2), angle, 1)

    Rtif = gdal.Open(folder + imagename + '_B2.TIF')
    Gtif = gdal.Open(folder + imagename + '_B3.TIF')
    Btif = gdal.Open(folder + imagename + '_B4.TIF')
    rgbArray = np.zeros((rows, cols, 3), 'uint16')
    gc.collect()
    rgbArray[ ..., 0 ] = Rtif.ReadAsArray()
    rgbArray[ ..., 1 ] = Btif.ReadAsArray()
    rgbArray[ ..., 2 ] = Gtif.ReadAsArray()
    grayImg = np.dot(rgbArray[ ..., :3 ], [ 0.299, 0.587, 0.114 ])
    del rgbArray
    gc.collect()

    transformedH = cv2.warpAffine(HImg, transformation, (cols, rows))
    transformedGray = cv2.warpAffine(grayImg, transformation, (cols, rows))
    '''
    ### Cropping ###
    '''
    transformedH = transformedH[ 780:6780, 780:6780 ]
    transformedGray = transformedGray[ 780:6780, 780:6780 ]
    '''
    ### Cropping to patches
    '''
    print('Cropping to patches')
    patches = np.append(
        np.expand_dims(get_patches(transformedGray, _PATCH_SIZE_), axis=1),
        np.expand_dims(get_patches(transformedH, _PATCH_SIZE_), axis=1),
        axis=1)
    print('Writing\n\n')
    with open(os.path.join(folder, 'batches/location_' + str(loc_idx + 1)), 'wb') as f:
        pickle.dump(dict(locationName=imagename, data=patches), f)
    gc.collect()

