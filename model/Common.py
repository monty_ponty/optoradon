import os
import random

import numpy as np

data_dir = '/home/dmitriy/projects/MetricLearning/'


def unpickle(file):
    import pickle
    fo = open(file, 'rb')
    dict = pickle.load(fo,encoding='latin1')
    fo.close()
    return dict


def load_data():
    xs = []
    ys = []
    for j in range(5):
        print(j)
        d = unpickle(os.path.join(data_dir, 'cifar-10-batches-py/data_batch_' + str(j + 1)))
        x = d['data']
        y = d['labels']
        xs.append(x)
        ys.append(y)

    d = unpickle(os.path.join(data_dir, 'cifar-10-batches-py/test_batch'))
    xs.append(d['data'])
    ys.append(d['labels'])

    x = np.concatenate(xs) / np.float32(255)
    y = np.concatenate(ys)
    x = np.dstack((x[:, :1024], x[:, 1024:2048], x[:, 2048:]))
    x = x.reshape((x.shape[0], 32, 32, 3)).transpose(0, 3, 1, 2)

    # subtract per-pixel mean
    pixel_mean = np.mean(x[0:50000], axis=0)
    # pickle.dump(pixel_mean, open("cifar10-pixel_mean.pkl","wb"))
    x -= pixel_mean

    # create mirrored images
    X_train = x[0:50000, :, :, :]
    Y_train = y[0:50000]
    X_train_flip = X_train[:, :, :, ::-1]
    Y_train_flip = Y_train
    X_train = np.concatenate((X_train, X_train_flip), axis=0)
    Y_train = np.concatenate((Y_train, Y_train_flip), axis=0)

    X_test = x[50000:, :, :, :]
    Y_test = y[50000:]

    return dict(
        X_train=X_train.astype('float32'),
        Y_train=Y_train.astype('int32'),
        X_test=X_test.astype('float32'),
        Y_test=Y_test.astype('int32'), )


def create_pairs(x, digit_indices):
    pairs = []
    labels = []
    n = min([len(digit_indices[d]) for d in range(10)]) - 1
    for d in range(10):
        for i in range(n):
            z1, z2 = digit_indices[d][i], digit_indices[d][i + 1]
            pairs += [[x[z1], x[z2]]]
            inc = random.randrange(1, 10)
            dn = (d + inc) % 10
            z1, z2 = digit_indices[d][i], digit_indices[dn][i]
            pairs += [[x[z1], x[z2]]]
            labels += [1, 0]
    return np.array(pairs, dtype="float32"), np.array(labels, dtype="int32")


def get_train_test_pairs():
    data = load_data()

    X_train = data['X_train']
    Y_train = data['Y_train']
    X_test = data['X_test']
    Y_test = data['Y_test']

    digit_indices = [np.where(Y_train == i)[0] for i in range(10)]
    tr_pairs, tr_y = create_pairs(X_train, digit_indices)
    digit_indices = [np.where(Y_test == i)[0] for i in range(10)]
    te_pairs, te_y = create_pairs(X_test, digit_indices)

    return tr_pairs, tr_y, te_pairs, te_y


def iterate_minibatches(a, b, targets, batchsize):
    indices = np.arange(len(targets), dtype="int32")
    for start_idx in range(0, len(targets) - batchsize + 1, batchsize):
        excerpt = indices[start_idx:start_idx + batchsize]
        yield a[excerpt], b[excerpt], targets[excerpt]