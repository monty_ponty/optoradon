import os
import tarfile
import gdal
import sys
import gc
import cv2
import numpy as np
import pickle
import matplotlib.pyplot as plt

folder = '/home/dmitriy/projects/MetricLearning/data/'
imagename='LC81920422016224LGN00_B4.TIF'
dataset = gdal.Open(folder + imagename )

print( 'Driver: ', dataset.GetDriver().ShortName,'/', \
      dataset.GetDriver().LongName)
print ('Size is ',dataset.RasterXSize,'x',dataset.RasterYSize, \
      'x',dataset.RasterCount)
print ('Projection is ',dataset.GetProjection())
geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print( 'Origin = (',geotransform[0], ',',geotransform[3],')')
    print ('Pixel Size = (',geotransform[1], ',',geotransform[5],')')

band = dataset.GetRasterBand(1)
print ('Band Type=',gdal.GetDataTypeName(band.DataType))
min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
    (min,max) = band.ComputeRasterMinMax(1)
print ('Min=%.3f, Max=%.3f' % (min,max))
if band.GetOverviewCount() > 0:
    print( 'Band has ', band.GetOverviewCount(), ' overviews.')
if not band.GetRasterColorTable() is None:
    print ('Band has a color table with ', \
    band.GetRasterColorTable().GetCount(), ' entries.')

import osr
from pyproj import Proj, transform
srs=osr.SpatialReference()
srs.ImportFromWkt(dataset.GetGCPProjection())
inProj = Proj(str(srs.ExportToProj4()))
outProj = Proj(init='epsg:4326')
x1,y1 =  geotransform[0], geotransform[3]
lon,lat = transform(inProj,outProj,x1,y1)
print (lat,lon)

for filename in os.listdir(folder):
    if filename.endswith(".TIF"):
        dataset = gdal.Open(folder + filename)
        geotransform = dataset.GetGeoTransform()
        srs = osr.SpatialReference()
        print(folder + filename)
        srs.ImportFromWkt(dataset.GetProjection())
        inProj = Proj(str(srs.ExportToProj4()))
        outProj = Proj(init='epsg:4326')
        x1, y1 = geotransform[ 0 ], geotransform[ 3 ]
        lon, lat = transform(inProj, outProj, x1, y1)
        print(lat, lon)

import os
import tarfile
import gdal
import sys
import gc
import cv2
import numpy as np
import pickle
import matplotlib.pyplot as plt
from PIL import Image
dataset = gdal.Open('/home/dmitriy/Downloads/sent/s1b-iw-grd-vv-20161204t053948-20161204t054013-003242-005868-001.tiff' )
arr=dataset.ReadAsArray()
arr=arr/arr.max()*255
gc.collect()
arr=arr.astype('uint8')
gc.collect()
im = Image.fromarray(arr)
im.save("/home/dmitriy/Downloads/sent/your_file.jpeg")
