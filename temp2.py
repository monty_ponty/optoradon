for dir in dirs:
    f = folder + dir + '/'
    dataset = None
    for filename in os.listdir(f):
        dataset = gdal.Open(os.path.join(f, filename))
        break
    gt = dataset.GetGeoTransform()
    west = gt[ 0 ]
    north = gt[ 3 ]
    east = west + gt[ 1 ] * dataset.RasterXSize
    south = north + gt[ 5 ] * dataset.RasterYSize
    _, _, z, l = utm.from_latlon((north + south) / 2, (west + east) / 2)

for dir in dirs:
    f = folder + dir + '/' + 'measurement/'
    print(folder + dir + '/')
    dataset = None
    for filename in os.listdir(f):
        dataset = gdal.Open(os.path.join(f, filename))
        break
    geoCtrlPoints = { (gcp.GCPLine, gcp.GCPPixel): [ gcp.GCPY, gcp.GCPX ] for gcp in dataset.GetGCPs() }
    rowRange = (0, dataset.RasterYSize - 1)
    colRange = (0, dataset.RasterXSize - 1)
    topLeftLL = geoCtrlPoints[ (rowRange[ 0 ], colRange[ 0 ]) ]
    topRightLL = geoCtrlPoints[ (rowRange[ 0 ], colRange[ 1 ]) ]
    bottomLeftLL = geoCtrlPoints[ (rowRange[ 1 ], colRange[ 0 ]) ]
    bottomRightLL = geoCtrlPoints[ (rowRange[ 1 ], colRange[ 1 ]) ]

    centreLat = (topLeftLL[ 0 ] + topRightLL[ 0 ] + bottomLeftLL[ 0 ] + bottomRightLL[ 0 ]) / 4
    centreLong = (topRightLL[ 1 ] + bottomRightLL[ 1 ] + topLeftLL[ 1 ] + bottomLeftLL[ 1 ]) / 4
    easting, northing, zone, zoneL = utm.from_latlon(centreLat, centreLong)
    with open(os.path.join(folder + dir + '/', str(zone) + str(zoneL) + '.txt'), 'w') as file:
        file.write(' ')
        pass

for optname in filedict:
    print(optname)
    opt = imread(os.path.join(folder, optname))
    sar = imread(os.path.join(folder, filedict[ optname ]))
    n, m, _ = opt.shape
    n = int(n / PSIZE)
    m = int(m / PSIZE)
    for i in range(1, n+1):
        for j in range(1, m+1):
            print('{} {} {} {}'.format((i - 1) * PSIZE, i * PSIZE, (j - 1) * PSIZE, j * PSIZE))
            imsave(os.path.join(folder, 'patch_OPT' + str(index) + '.png'), \
                   opt[ (i - 1) * PSIZE:i * PSIZE, (j - 1) * PSIZE:j * PSIZE, :3 ])
            imsave(os.path.join(folder, 'patch_SAR' + str(index) + '.png'), \
                   sar[ (i - 1) * PSIZE:i * PSIZE, (j - 1) * PSIZE:j * PSIZE, :3 ])
            index = index + 1

            print(index)


